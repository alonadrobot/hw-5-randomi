package com.random;

import java.util.Random;

public class random {
    private static Random random = new Random();
    public static void main(String[] args) {
        System.out.println("Рандомное число: "+getRandom());
        System.out.print("10 рандомных чисел:");
        for (int i = 0; i < 10; i++) {
            System.out.print("  "+getRandom());
        }
        System.out.print("\n10 рандомных чисел в диапазоне от 0 до 10:");
        for (int i = 0; i < 10; i++) {
            System.out.print("  "+getRandomRange(10,0));
        }
        System.out.print("\n10 рандомных чисел в диапазоне от 20 до 50:");
        for (int i = 0; i < 10; i++) {
            System.out.print("  "+getRandomRange(50,20));
        }
        System.out.print("\n10 рандомных чисел в диапазоне от -10 до 10:");
        for (int i = 0; i < 10; i++) {
            System.out.print("  "+getRandomRange(20,-10));
        }
        System.out.print("\nВывести случайное количество(от 3 до 15) рандомных чисел в диапазоне от -10 до 35:");
        for (int i = 0; i <getRandomRange(15,3) ; i++) {
            System.out.print("  "+getRandomRange(45,-10));
        }
    }
    private static int getRandom(){
        return random.nextInt();
    }
    private static int getRandomRange(int max, int min){
        if(min<0){
            return random.nextInt(max)+min;
        }
        int i=random.nextInt(max-min);
        return min+=i;
    }
}
